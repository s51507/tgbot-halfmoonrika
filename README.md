# tgbot-halfmoonrika
tgbot-halfmoonrika

## Install

```bash
npm install --save tgbot-halfmoonrika
```

## prototype

```bash
yagop/node-telegram-bot-api
```
[https://github.com/yagop/node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api)

## Update

```bash
v0.2.01 - add youtube-dl
          add admin mode

v0.2.00 - add error message on console
          add Airtable (can random picture , like database)
          add Youtube search , get video picture
          add my work data (don't ask,you will be afraid)
          clear some message when you start (console)

v0.1.03 - sort out,prepare for use DB

v0.1.02 - add inline keyboard at picture upload for check yes/no

v0.1.01 - removed something

v0.1.00 - add keyboard, inline keyboard
          add reset tgphoto.txt
          add picture upload (only I can use ( ﾟ ∀ 。 ) )

v0.0.06 - add async module
          add close bot function "走開" (but can't restart)

v0.0.05 - use tg photo.file_id post image (faster)
          testing use bot to reset tgphoto.txt

v0.0.04 - use imgur post images (so slooooooooooooooooooow)

v0.0.03 - sort out

v0.0.02 - fix hawk security vulnerability

v0.0.01 - add imgur
```
