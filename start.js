process.env["NTBA_FIX_319"] = 1;
//setting
var setting = require("./modules/setting.js");
var TelegramBot = require('node-telegram-bot-api');
var token = setting.tg_token;
//var cid = '-187569588'; //BOT_TEST
//使用Long Polling的方式與Telegram伺服器建立連線
var bot = new TelegramBot(token, {
  polling: true
});
//imgur
var imgur = require("./modules/imgur.js");
//資料讀取
var tgphoto = require("./modules/tgphoto.js");
//同步function
var async = require('async');
//隨機
var GetRandomNum = require("./modules/GetRandomNum.js");
//keyboard
var keyboard = require("./modules/keyboard.js");
//database
var airtable = require("./modules/airtable.js");
//youtube
var youtube = require("./modules/youtube.js");
//youtube-dl
var ytdl = require("./modules/youtube-dl.js");
//callbacklist
var callbacklist = require("./modules/callbacklist.js");
//fish
var fish = require("./modules/fish.js");

//上傳MODE
var mode = 0;

//-------------------------------------------------------------------
//DeBug
bot.on('polling_error', (error) => {
  if (error.code == 'EFATAL') {
    console.log('EFATAL : network error');
  }
  if (error.code == 'ETELEGRAM') {
    console.log('error was returned from Telegram servers');
  } else {
    console.log(error.code);
  }
});

bot.on('webhook_error', (error) => {
  if (error.code == 'EPARSE') {
    console.log('EPARSE : response body could not be parsed');
  } else {
    console.log(error.code);
  }
});

//-------------------------------------------------------------------
//Start
bot.sendMessage('-187569588', '(<ゝω・) 綺羅星☆'); //BOT_TEST
bot.sendMessage('665338028', '(<ゝω・) 綺羅星☆'); //個人
//bot.sendSticker('-246400085', 'CgADBQADQgADEDzoVG3bg4DLCPwBAg');//三人
//-------------------------------------------------------------------
//偵測文字
//功能指令

//計算
bot.onText(/\/cal (.+)/, function(msg, match) {
  var chatId = msg.chat.id;
  var resp = match[1].replace(/[^-()\d/*+.]/g, '');
  // match[1]的意思是 /cal 後面的所有內容
  resp = '計算結果為: ' + eval(resp);
  // eval是用作執行計算的function
  bot.sendMessage(chatId, resp); //發送訊息的function
});

//Youtube
bot.onText(/\/yt (.+)/, function(msg, match) {
  var chatId = msg.chat.id;
  var resp = match[1];

  ytsearch(chatId, resp);
});
//影片封面圖
bot.onText(/\/ytpic (.+)/, function(msg, match) {
  var chatId = msg.chat.id;
  var resp = match[1];
  youtube.getpic(resp, function(callback) {
    bot.sendMessage(chatId, callback);
  })
});

//影片下載
bot.onText(/\/ytdl (.+)/, function(msg, match) {
  var chatId = msg.chat.id;
  var resp = match[1];
  ytdl.dl(resp, function(callback) {
    var title = callback;
    async.series(
      [
        function(callback) {
          bot.sendMessage(chatId, title + " 上傳中...，請稍後一分鐘左右" + "\n" + "Σ(ﾟДﾟ；≡；ﾟдﾟ)");
          callback(null, 'A');
        },
        function(callback) {
          setTimeout(function() {
            callback(null, 'B');
          }, 20000)
        }
      ],
      function(err, results) {
        bot.sendVideo(chatId, './youtube-dl/' + title + '.mp4');
        if (err) {
          bot.sendMessage(chatId, "出錯啦!! ヾ(;ﾟ;Д;ﾟ;)ﾉﾞ");
        }
      }
    )
  })
});

//-------------------------------------------------------------------
//指定文字
bot.on('message', message => {
  const chatId = message.chat.id;
  var text = message.text;

  //文字
  if (text == '/?') {
    bot.sendMessage(chatId,
      '/yt    想搜尋的東東    - youtube搜尋 \n' +
      '/ytpic youtube影片網址 - 取得影片封面圖片 \n' +
      '/ytdl  youtube影片網址 - 下載影片');
    return;
  }
  if (text == '/start') {
    bot.sendMessage(chatId, '你好啊，嘿嘿嘿 (´≖◞౪◟≖)');
    return;
  }
  if (text == '喔') {
    bot.sendMessage(chatId, '怎麼?想被督?  (΄◞ิ౪◟ิ‵)');
    return;
  }
  if (text == '不要') {
    bot.sendMessage(chatId, '聽話!讓我看看! ლ(◉◞౪◟◉ )ლ');
    return;
  }
  if (text == '吃啥') {
    bot.sendMessage(chatId, '吃我的大雞雞啦 థ౪థ');
    return;
  }
  if (text == 'adminon') {
    mode = 1;
    bot.sendMessage(chatId, '(」・ω・)」MODEー！\n(／・ω・)／ ON!ー！');
    return;
  }
  if (text == 'adminoff') {
    mode = 0;
    bot.sendMessage(chatId, '(／・ω・／)」MODEー！\n(」・ω・)」 OFF!ー！');
    return;
  }
  //-----------------------------------------------------------------
  //貼圖
  if (text == '咕嚕') {
    bot.sendMessage(chatId, '咕嚕靈波 （●´ ∀｀）ノ ♡');
    bot.sendSticker(chatId, 'CgADBQADQgADEDzoVG3bg4DLCPwBAg');
    return;
  }
  if (text == 'what') {
    bot.sendSticker(chatId, 'CAADBQAD6gADxGCaD0dLu1-KpiIDAg');
    return;
  }
  if (text == '沒電話') {
    bot.sendSticker(chatId, 'CAADBQAEAQACxGCaD3xt__Pet7yWAg');
    return;
  }
  if (text == '無聊') {
    bot.sendSticker(chatId, 'CAADBQAEAQACxGCaD3xt__Pet7yWAg');
    return;
  }
  if (text == '三小') {
    bot.sendSticker(chatId, 'CAADAQADpgQAAmbKaAmeve7gA8uZFwI');
    return;
  }
  if (text == 'fuck') {
    for (var i = 0; i < 10; i++) {
      bot.sendSticker(chatId, 'CgADBQADgQADJ4aoVQABnGZjzS5mNAI');
    }
    return;
  }
  //-----------------------------------------------------------------
  //imgur
  //單張
  if (text == 'img') {
    imgur.img(function(img) {
      bot.sendPhoto(chatId, img);
    });
    return;
  }
  //多張抽一
  if (text == 'imgs') {
    imgur.imgs(function(imgs) {
      bot.sendPhoto(chatId, imgs);
    });
  }
  //-----------------------------------------------------------------
  //資料讀取TXT
  //抽照片-吉岡里帆
  if (text == '抽') {
    bot.sendPhoto(chatId, tgphoto.pics());
    return;
  }
  //-----------------------------------------------------------------
  //線上資料庫 - Airtable
  //抽照片-吉岡里帆
  if (text == 'db') {
    airtable.ranpic(function(callback) {
      bot.sendPhoto(chatId, callback);
    });
    return;
  }
  //-----------------------------------------------------------------
  //KeyBoard
  if (text == 'testkeyboard') {
    bot.sendMessage(chatId, 'inlinekeyboard', keyboard.test1).then(function(sended) {});
  }
  if (text == 'testkeyboard2') {
    var tt = bot.sendMessage(chatId, 'keyboard', keyboard.test2);
  }
  if ((message.reply_to_message != undefined) && (message.reply_to_message.from.id == '653196211')) {
    const chatId = message.chat.id;
    var msgId = message.reply_to_message.message_id;
    bot.sendMessage(chatId, '收到 ヽ(・×・´)ゞ', keyboard.remove);
    bot.deleteMessage(chatId, msgId);
  }
  if (text == 'resetpic') {
    bot.sendMessage(chatId, '確定要重置抽圖資料庫? Σヽ(ﾟД ﾟ; )ﾉ', keyboard.resetpic).then(function(sended) {});
  }
  if (text == '走開') {
    async.series(
      [
        function(callback) {
          bot.sendMessage(chatId, 'I will be back ლ(◉◞౪◟◉ )ლ');
          callback(null, 'A');
        },
        function(callback) {
          setTimeout(function() {
            callback(null, 'B');
          }, 3000)
        }
      ],
      function(err, results) {
        process.exit();
      }
    )
  }
});
//-------------------------------------------------------------------
//偵測貼圖
bot.on('sticker', message => {
  const chatId = message.chat.id;
  var sid = message.sticker.file_id;
  if (sid == 'CAADAQADpgQAAmbKaAmeve7gA8uZFwI') {
    bot.sendMessage(chatId, '三小?');
    return;
  }
});
/*
//查詢貼圖代碼
bot.on('sticker', message => {
  const chatId = '665338028';
  var text = message.sticker.file_id;
  bot.sendMessage(chatId, text);
  return;
});
*/
//查詢圖片代碼
bot.on('photo', message => {
  const chatId = '665338028';
  var imgId = message.photo[0].file_id;
  bot.sendMessage(chatId, message.from.first_name + '\n' + imgId);
  return;
});

//-------------------------------------------------------------------
//偵測圖片
//加入圖片、查詢圖片代碼
bot.on('photo', message => {
  const chatId = message.chat.id;
  var imgId = message.photo[0].file_id;
  var userId = message.from.id;
  if (userId == 665338028 && mode == 1) {
    bot.sendPhoto(chatId, imgId, keyboard.checkpic);
    tgphoto.getpicId(imgId);
    return;
  } else {
    console.log(message.from.first_name + ' is uploadPic.');
    return;
  }
  return;
});
//-------------------------------------------------------------------
//偵測callback
bot.on('callback_query', message => {
  var chatId = message.message.chat.id;
  var data = message.data;
  var msgId = message.message.message_id;

  callbacklist.check(data, function(callback) {
    if (data.substring(0, 8) == 'checkpic') {
      bot.editMessageCaption(callback, {
        chat_id: chatId,
        message_id: msgId
      });
    } else if (data == 'ytsnext') {
      ytsearch(chatId, youtube.getoldId());
      bot.editMessageText(callback, {
        chat_id: chatId,
        message_id: msgId
      });
    } else {
      bot.editMessageText(callback, {
        chat_id: chatId,
        message_id: msgId
      });
    }
  });
});
//-------------------------------------------------------------------
//function
//ytytsearch - 要重複執行，所以就獨立出來了
function ytsearch(chatId, resp) {

  async.series(
    [
      function(callback) {
        youtube.search(resp, function(callback) {
          bot.sendMessage(chatId, callback);
        })
        callback(null, 'A');
      },
      function(callback) {
        setTimeout(function() {
          callback(null, 'B');
        }, 3000)
      }
    ],
    function(err, results) {
      bot.sendMessage(chatId, '還想要' + resp + '嗎? ･ิ≖ ω ≖･ิ✧', keyboard.ytsearch);
    }
  )
}


//-------------------------------------------------------------------
//Other
bot.onText(/\/echo (.+)/, (message, match) => {

  const chatId = message.chat.id;
  const resp = match[1]; // the captured "whatever"

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, resp);
});
//-------------------------------------------------------------------