//setting
var setting = require("./setting.js");
var Airtable = require('airtable');
var base = new Airtable({
  apiKey: setting.airtable_key
}).base('appgai9koufo0RSF3');
//隨機
var GetRandomNum = require("./GetRandomNum.js");
//資料讀取
var fs = require('fs');
var tgphoto = './modules/tgphoto.txt';

//抽
function ranpic(callback) {
  base('tgbot-uploadpic').select({
    view: "Grid view"
  }).eachPage(function page(records, fetchNextPage) {
      var n = GetRandomNum.random(0, records.length - 1);
      callback(records[n].get('photoId'));
      fetchNextPage();
    },
    function done(err) {});
}

//重置 - 測試中，順序會亂掉，看得很不爽，可能要用同步
function resetpic() {

  var data = fs.readFileSync(tgphoto, 'utf-8');
  var arr = [];
  arr = data.toString().split('\n');

  arr.forEach(function(arr) {
    if (arr !== '') {
      console.log(arr);
      base('tgbot-uploadpic').create({
        "photoId": arr,
        "userId": "665338028",
      }, function(err, record) {

      });
    }
  });
}

module.exports = {
  ranpic: ranpic
}