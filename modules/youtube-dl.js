const fs = require('fs');
const ytdl = require('ytdl-core');

var title = '';

function dl(url, callback) {
  ytdl.getInfo(url, (err, info) => {
    console.log(info.player_response.videoDetails.title);
    title = info.player_response.videoDetails.title;
    ytdl(url, {
        filter: (format) => format.container === 'mp4',
      })
      .pipe(fs.createWriteStream('./youtube-dl/' + title.replace(/[\\/:*?"<>|]/g, '-') + '.mp4'));
    callback(title.replace(/[\\/:*?"<>|]/g, '-'));
  });
}



module.exports = {
  dl: dl
}