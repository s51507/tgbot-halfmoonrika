//setting
var setting = require("./setting.js");
//imgur單張
var http = require("https");
var image_options = {
  "method": "GET",
  "host": "api.imgur.com",
  "path": "/3/image/altj8a9",
  "headers": {
    "Authorization": setting.imgur_headers
  },
  json: true
};

//imgur album tgbot
var album_options = {
  "method": "GET",
  "host": "api.imgur.com",
  "path": "/3/album/bucucgx/images",
  "headers": {
    "Authorization": setting.imgur_headers
  },
  json: true
};

//隨機
var GetRandomNum = require("./GetRandomNum.js");


function img(callback) {
  var req = http.request(image_options, function(res) {
    var chunks = [];
    res.on("data", function(chunk) {
      chunks.push(chunk);
    });
    res.on("end", function() {
      var body = Buffer.concat(chunks);
      var test2 = JSON.parse(body);
      callback(test2.data.link);
    });
  });
  req.end();
}

function imgs(callback) {
  var req = http.request(album_options, function(res) {
    var chunks = [];
    res.on("data", function(chunk) {
      chunks.push(chunk);
    });
    res.on("end", function() {
      var body = Buffer.concat(chunks);
      var test2 = JSON.parse(body);
      var num = 0; //照片總張數
      test2.data.forEach(function(element) {
        num += 1;
      });
      var rnum = GetRandomNum.random(0, num - 1); //亂數選擇一張
      callback(test2.data[rnum].link);
    });
  });
  req.end();
}

module.exports = {
  img: img,
  imgs: imgs
}