var fs = require('fs');
var tgphoto = './modules/tgphoto.txt';
var GetRandomNum = require('./GetRandomNum.js');
var picId = '';

function getpicId(file_id) {
  picId = file_id;
}

function pics() {
  var data = fs.readFileSync(tgphoto, 'utf8');
  var arr = [];
  arr = data.toString().split('\n');
  var rnum = GetRandomNum.random(0, arr.length - 2);
  return arr[rnum];
}

function uploadPic() {
  fs.appendFile(tgphoto, picId + '\n', function(err) {
    console.log('uploadPic');
  });
}

//重置tgphoto.txt
function yesReset() {
  fs.writeFile(tgphoto,
    'AgADBQADBKgxG1iLiFQRxjj7HbgTaU2x1jIABKjExfvkjyOAZfEBAAEC' + '\n' + //1
    'AgADBQADDagxG4L-iVQSwC3ljhZyrwFQ1TIABDfJtH5UyO--vmoDAAEC' + '\n' + //2
    'AgADBQADDqgxG4L-iVRKVcPDlEVzM8pB1jIABPdGf1_FyuqNL2oDAAEC' + '\n' + //3
    'AgADBQADBagxG1iLiFSaxJw_7_iQSsEx1TIABNBNss2XO47jfXcDAAEC' + '\n' + //4
    'AgADBQADD6gxG4L-iVSRay6Rx51EYOpB1jIABNb_3SWkB82AH2kDAAEC' + '\n' + //5
    'AgADBQADBqgxG1iLiFToJwIbiYHgk-Ar1TIABJCXebavboTRuXQDAAEC' + '\n' + //6
    'AgADBQADEKgxG4L-iVRWhcjWJo5WVyWh1jIABA51SUvpeCtWsOgBAAEC' + '\n' + //7
    'AgADBQADEagxG4L-iVQfB46AaiOeLyCu1jIABECBJlGSdTlFeO4BAAEC' + '\n' + //8
    'AgADBQADEqgxG4L-iVQOmhVO2GOz_w4v1TIABH5GaZnHHu_BFXADAAEC' + '\n' + //9
    'AgADBQADB6gxG1iLiFSOIbkkD5xtiYow1TIABEqv-qpqy2qjQG0DAAEC' + '\n' + //10
    'AgADBQADE6gxG4L-iVR4uIZMRTru7sqp1jIABasti_9qwLgR7AEAAQI' + '\n' + //11
    'AgADBQADFKgxG4L-iVTJsgPRlUWPyaJO1TIABFg9p80KDfsCjWgDAAEC' + '\n' + //12
    'AgADBQADJ6gxG6hD0VQzLljjXK0o9nUw1TIABMvVUSjUWWiE3akDAAEC' + '\n' + //13
    'AgADBQADK6gxG6hD0VRyFO6Rj7kHujco1TIABPlwjpXUwzVUZagDAAEC' + '\n' + //14
    'AgADBQADLKgxG6hD0VQyrETG9qcElrMw1TIABGyME04PQ9Ef36QDAAEC' + '\n' + //15
    'AgADBQADSagxG8PE0VQYgF7xSUmJix891jIABOFVd83zMhBhRJUDAAEC' + '\n' + //16
    'AgADBQADSqgxG8PE0VRzqhq8Dh9a-URP1TIABElK0IzXIyszNpkDAAEC' + '\n' + //17
    'AgADBQADS6gxG8PE0VQBIORA-Uvc0NAv1TIABGrlKuYRvEYhqp8DAAEC' + '\n' + //18
    'AgADBQADTKgxG8PE0VTI_6xMR0Hu7jWw1jIABHsJKm67BVkiix0CAAEC' + '\n' + //19
    'AgADBQADMagxG6hD0VTqbqYlHXjqJrOe1jIABOEP1Mbo8lSJwh4CAAEC' + '\n' + //20
    'AgADBQADMqgxG6hD0VQH0nsIoyaUHE9B1jIABAoHX577KiYfF5wDAAEC' + '\n' + //21
    'AgADBQADTagxG8PE0VQRhXDzLhIiikAr1TIABA-UKNQPf-Qlt6oDAAEC' + '\n' + //22
    'AgADBQADN6gxG6hD0VQ2owZ55xr7An8n1TIABK1EOQV8H3Fc5qgDAAEC' + '\n' + //23
    'AgADBQADTqgxG8PE0VTe3OyIqEmHxq2v1jIABPjbNwVXS_Vi4yMCAAEC' + '\n' + //24
    'AgADBQADOagxG6hD0VQEdWi0HkBsE4Ki1jIABDcQzS_IiNB8axsCAAEC' + '\n' + //25
    'AgADBQADOqgxG6hD0VRk37_5n85BhoJR1TIABPOxeY__OuNdRJoDAAEC' + '\n' + //26
    'AgADBQADSqgxG6hD0VTWuISqyYLLLNmg1jIABPCSEFh0M-eEeSACAAEC' + '\n', //27
    function(err) {
      console.log('resetpic');
    });
}

module.exports = {
  getpicId: getpicId,
  pics: pics,
  uploadPic: uploadPic,
  yesReset: yesReset
}