var tgphoto = require('./tgphoto.js');
var youtube = require("./youtube.js");

function check(data, callback) {
  switch (data) {
    case 'doreset':
      tgphoto.yesReset();
      callback("已重置 ( × ω × )");
      break;
    case 'noreset':
      callback("已取消 (*´∀`)~♥");
      break;
    case 'checkpicyes':
      tgphoto.uploadPic();
      callback("上傳成功 ｡:.ﾟヽ(*´∀`)ﾉﾟ.:｡");
      break;
    case 'checkpicno':
      callback("好吧... (｡í _ ì｡)");
      break;
    case 'ytsnext':
      callback("拿去啦! (ﾉ > ω <)ﾉ");
      break;
    default:
      callback("你點了 '" + data + "'");
  }
}

module.exports = {
  check: check
}