//InlineKeyboardMarkup
var test1 = {

  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{
        text: 'Some button text 1',
        callback_data: '1'
      }, {
        text: 'Some button text 2',
        callback_data: '2'
      }],
      [{
        text: 'Some button text 3',
        callback_data: '3'
      }],
      [{
        text: 'Some button text 4',
        callback_data: '4'
      }]
    ]
  })
};

//ReplyKeyboardMarkup
var test2 = {
  reply_markup: JSON.stringify({
    keyboard: [
      [{
        text: 'Some button text 1'
      }],
      [{
        text: 'Some button text 2'
      }]
    ],
    resize_keyboard: true,
    one_time_keyboard: true,
    selective: false
  })
};

//ReplyKeyboardRemove
var remove = {
  reply_markup: JSON.stringify({
    remove_keyboard: true,
    selective: false
  })
};

//InlineKeyboard
//重設抽圖資料庫
var resetpic = {
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{
        text: '重置抽圖資料庫  (☍﹏⁰。) ',
        callback_data: 'doreset'
      }],
      [{
        text: '取消 (๑´ㅁ`)',
        callback_data: 'noreset'
      }]
    ]
  })
};

//確認圖片上傳
var checkpic = {
  caption: '確定要上傳這張照片? (,,・ω・,,)',
  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{
        text: '確認  ლ( ╹ ◡ ╹ ლ) ',
        callback_data: 'checkpicyes'
      }],
      [{
        text: '取消 ( º ﹃ º )',
        callback_data: 'checkpicno'
      }]
    ]
  })
};


var ytsearch = {

  reply_markup: JSON.stringify({
    inline_keyboard: [
      [{
        text: '我還要，快給我 _(┐「﹃ﾟ｡)_',
        callback_data: 'ytsnext'
      }]
    ]
  })
};


module.exports = {
  test1: test1,
  test2: test2,
  remove: remove,
  resetpic: resetpic,
  checkpic: checkpic,
  ytsearch: ytsearch
}