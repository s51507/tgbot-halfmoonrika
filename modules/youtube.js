//setting
var setting = require("./setting.js");
var YouTube = require('youtube-node');
var youTube = new YouTube();

youTube.setKey(setting.youtube_key);

var pageToken = '';
var oldId = '';

function getoldId() {
  return oldId;
}

function search(Id, callback) {

  if (Id != oldId) {
    pageToken = '';
  }

  youTube.search(Id, 3, {
    pageToken: pageToken
  }, function(error, result) {
    result.items.forEach(function(data) {
      if (data.id.kind == 'youtube#video') {
        callback(data.snippet.title + ' \n ' + 'https://youtu.be/' + data.id.videoId);
      }
      if (data.id.kind == 'youtube#channel') {
        callback(data.snippet.title + ' \n ' + 'https://www.youtube.com/channel/' + data.id.channelId);
      }
      if (data.id.kind == 'youtube#playlist') {
        callback(data.snippet.title + ' \n ' + 'https://www.youtube.com/playlist?list=' + data.id.playlistId);
      }
    });
    pageToken = result.nextPageToken;
    oldId = Id;
  });
}

function getpic(Id, callback) {
  youTube.getById(Id, function(error, result) {
    if (!result.items[0]) {
      callback('打錯了啦 ヽ(#`Д´)ﾉ');
    } else {
      callback(result.items[0].snippet.thumbnails.high.url);
    }
  });
}



module.exports = {
  getoldId: getoldId,
  search: search,
  getpic: getpic
}