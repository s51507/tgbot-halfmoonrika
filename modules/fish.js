var request = require('request');
var GetRandomNum = require('./GetRandomNum.js');

var headers = {
  'Content-Type': '',
  'Authorization': ''
};
var host = '';
var token = '';

function help() {
  return (
    "創建帳號 " + "\n" +
    "/add 帳號,密碼" + "\n" +
    "———————————————" + "\n" +
    "登入 " + "\n" +
    "/login 帳號,密碼" + "\n" +
    "———————————————" + "\n" +
    "登出 " + "\n" +
    "/logout 帳號,密碼" + "\n" +
    "———————————————" + "\n" +
    "儲值 " + "\n" +
    "/in 帳號,儲值金額" + "\n" +
    "———————————————" + "\n" +
    "取款 " + "\n" +
    "/out 帳號,取款金額" + "\n" +
    "———————————————" + "\n" +
    "查餘額 " + "\n" +
    "/money 帳號" + "\n" +
    "———————————————" + "\n" +
    "取得遊戲網址 (要先登入)" + "\n" +
    "/game" + "\n" +
    "———————————————" + "\n" +
    "兩個參數中間一定要加逗號 (`・ω・´)");
}

//新增帳號
function create(res, callback) {
  var tmp = res.split(',');
  request.post({
    headers,
    url: host + '/game/create',
    form: {
      account: tmp[0],
      password: tmp[1]
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    callback(data.status.message);
  });
}

//登入
function login(res, callback) {
  var tmp = res.split(',');
  request.post({
    headers,
    url: host + '/game/login',
    form: {
      account: tmp[0],
      password: tmp[1]
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('帳號密碼打錯了拉 ヽ(#`Д´)ﾉ')
    } else {
      token = data.data.token;
      callback(data.data.token);
    }
  });
}

//登出
function logout(res, callback) {
  var tmp = res.split(',');
  request.post({
    headers,
    url: host + '/game/logout',
    form: {
      account: tmp[0],
      password: tmp[1]
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('帳號密碼打錯了拉 ヽ(#`Д´)ﾉ')
    } else {
      callback(data.status.message);
    }
  });
}

//儲值
function deposit(res, callback) {
  var rnum = GetRandomNum.random(1, 9999999);
  var tmp = res.split(',');
  request.post({
    headers,
    url: host + '/game/deposit',
    form: {
      mtcode: rnum,
      eventtime: Math.floor(Date.now() / 1000),
      account: tmp[0],
      amount: tmp[1]
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('帳號或金額打錯了拉 ヽ(#`Д´)ﾉ');
    } else {
      callback(data.data.balance);
    }
  });
}

//取款
function withdraw(res, callback) {
  var rnum = GetRandomNum.random(1, 9999999);
  var tmp = res.split(',');
  request.post({
    headers,
    url: host + '/game/withdraw',
    form: {
      mtcode: rnum,
      eventtime: Math.floor(Date.now() / 1000),
      account: tmp[0],
      amount: tmp[1]
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('帳號或金額打錯了拉 ヽ(#`Д´)ﾉ');
    } else {
      callback(data.data.balance);
    }
  });
}

//遊戲連結
function gamelink(callback) {
  request.post({
    headers,
    url: host + '/game/gamelink',
    form: {
      usertoken: token,
      gamecode: 'G1',
      lang: 'zh-CN'
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('沒登入就想玩膩 ヽ(#`Д´)ﾉ')
    } else {
      callback(data.data.link);
    }

  });
}

//查詢玩家餘額
function balance(id, callback) {
  request({
    headers,
    uri: host + '/game/balance/' + id,
    method: 'GET'
  }, function(err, res, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('帳號打錯了拉 ヽ(#`Д´)ﾉ')
    } else {
      callback(data.data.balance);
    }
  });
}

//=============================================================
//方便用
//登入
function loginian(res, callback) {
  request.post({
    headers,
    url: host + '/game/login',
    form: {
      account: res,
      password: res
    }
  }, function(err, httpResponse, body) {
    var data = JSON.parse(body);
    if (data.data == null) {
      callback('帳號密碼打錯了拉 ヽ(#`Д´)ﾉ')
    } else {
      token = data.data.token;
      callback(data.data.token);
    }
  });
}

module.exports = {
  help: help,
  create: create,
  login: login,
  logout: logout,
  deposit: deposit,
  withdraw: withdraw,
  gamelink: gamelink,
  balance: balance,
  loginian: loginian
}